﻿using System;

namespace Tabela
{
    public class Tabela
    {
        private int[] tabelita;

        private int n;

        private int elementos;


        public int[] Tabelita { get; set; }

        public int N { get; set; }

        public int Elementos { get; set; }


        public Tabela(int n)
        {
            N = n;
            Tabelita = new int[N];
            Elementos = 0;
        }

        public Tabela(int[] a)
        {
            Tabelita = a;
            N = a.Length;
            Elementos = N;
        }


        private bool IsFull()
        {
            return Elementos == N;
        }

        private bool IsEmpty()
        {
            return Elementos == 0;
        }

        public bool Add(int num)
        {
            if (IsFull())
            {
                return false;
            }

            Tabelita[Elementos] = num;
            Elementos++;
            return true;
        }

        public bool Add(int num, int pos)
        {
            if (IsFull())
            {
                return false;
            }

            if (pos > Elementos)
            {
                return false;
            }

            for (int i = Elementos; i > pos; i--)
            {
                Tabelita[i] = Tabelita[i - 1];
            }

            Tabelita[pos] = num;
            Elementos++;
            return true;
        }

        public override string ToString()
        {
            string aux = "";

            for (int i = 0; i < Elementos; i++)
            {
                aux = aux + String.Format("{0}\t", Tabelita[i]);
            }

            return aux;
        }
    }
}
