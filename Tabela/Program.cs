﻿using System;

namespace Tabela
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rd = new Random();

            Tabela t = new Tabela(20);

            for (int i = 0; i < 15; i++)
            {
                int num = rd.Next(100);
                if (t.Add(num))
                {
                    Console.WriteLine("Adicionado {0}", num);
                }
                else
                {
                    Console.WriteLine("Não pode ser adicionado {0}", num);
                }
            }

            Console.WriteLine(t.ToString());

            t.Add(500, 0);
            t.Add(600, 0);

            Console.WriteLine(t);

            Console.ReadKey();
        }
    }
}
